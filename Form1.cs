﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace hw19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            string[] lines = new string[2];
            string name = null, pass = null;
            bool inside = false;

            if (userName.Text != "" && password.Text != "")//not null
            {
                try
                {   // Open the text file using a stream reader.
                    using (StreamReader file = File.OpenText("Users.txt"))
                    {
                        string s = String.Empty;
                        while ((s = file.ReadLine()) != null)
                        {
                            name = null;
                            pass = null;

                            for (int i = 0; i < s.Length; i++)//for every line
                            {
                                if (s[i] != ',')
                                {
                                    name += s[i];
                                }
                                else
                                {
                                    for (i = i + 1; i < s.Length; i++)
                                    {
                                        pass += s[i];
                                    }

                                    if (userName.Text == name && password.Text == pass)//login is valid
                                    {
                                        MessageBox.Show("Welcome in!");
                                        Form2 f = new Form2();
                                        this.Hide();

                                        f.ShowDialog();

                                        this.Show();
                                        inside = true;
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception i)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(i.Message);
                }
                if (!inside)
                {
                    MessageBox.Show("User Name or Password are wrong!");
                }
            }
        }

        private void userName_TextChanged(object sender, EventArgs e)
        {

        }

        private void password_TextChanged(object sender, EventArgs e)
        {

        }

        private void quitButton_Click(object sender, EventArgs e)//"easter egg"
        {
            System.Drawing.Rectangle workingRectangle =
            Screen.PrimaryScreen.WorkingArea;

            // Set the size of the form slightly less than size of 
            // working rectangle.
            this.Size = new System.Drawing.Size(
                workingRectangle.Width - 10, workingRectangle.Height - 10);

            // Set the location so the entire form is visible.
            this.Location = new System.Drawing.Point(5, 5);
        }
    }
}
